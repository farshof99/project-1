@extends('layout.master')
@section('judul')
Edit Product
@endsection
@section('content')
<div class="card-body">

<form action="/produk/{{$produk->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Nama Produk</label>
      <input type="text" value="{{$produk->nama}}" name="nama" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group col-md-6">
      <label>Harga</label>
      <input type="text" value="{{$produk->harga}}" name="harga" class="form-control">
    </div>
    @error('harga')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>

        <div class="form-group">
            <label>Deskripsi</label>
            <textarea name="deskripsi" cols="10" rows="10" class="form-control">{{$produk->deskripsi}}</textarea>
        </div>
        @error('deskripsi')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Kategori</label>
            <select name="kategori_id" class="form-control" id="">
            <option value="">Pilih Kategori</option>
            @foreach($kategori as $item)
                @if ($item->id === $produk->kategori_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                <option value="{{$item->id}}" >{{$item->nama}}</option>
                @endif
            @endforeach
        </select>
    </div>


    <div class="form-group">
        <label>Foto Produk</label>
        <input type="file" name="foto" class="form-control" name="foto">
    </div>
    @error('foto')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  
  <button type="submit" class="btn btn-primary">Edit</button>
</form>


</div>
@push('script')
    
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>tinymce.init({selector:'textarea'});</script>
@endpush
@endsection
