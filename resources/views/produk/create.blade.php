@extends('layout.master')
@section('judul')
Our Product
@endsection
@section('content')
<div class="card-body">

<form action="/produk" method="POST" enctype="multipart/form-data">
    @csrf
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Nama Produk</label>
      <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group col-md-6">
      <label>Harga</label>
      <input type="text" name="harga" class="form-control">
    </div>
    @error('harga')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>

        <div class="form-group">
            <label>Deskripsi</label>
            <textarea name="deskripsi" cols="10" rows="10" class="form-control"></textarea>
        </div>
        @error('deskripsi')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
        <label>Kategori</label>
        <select name="kategori_id" class="form-control">
            <option value="">Pilih Kategori</option>
            @foreach($kategori as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
    </div>


    <div class="form-group">
        <label>Foto Produk</label>
        <input type="file" class="form-control" name="foto">
    </div>
    @error('foto')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  
  <button type="submit" class="btn btn-primary">Tambahkan</button>
</form>


</div>
@push('script')
    
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>tinymce.init({selector:'textarea'});</script>
@endpush
@endsection
