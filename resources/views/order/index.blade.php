@extends('layout.master')
@section('judul')
LIST ORDER
@endsection
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush

@section('content')
<div class="container">

    <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr style="text-align: center;">
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Harga</th>
            <th scope="col">Quantity</th>
            <th scope="col">Total</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($order as $key => $item)
              <tr style="text-align: center;">
                  <td>{{$key + 1}}</td>
                  <td>{{$item->produk->nama}}</td>
                  <td>{{$item->produk->harga}}</td>
                  <td>{{$item->quantity}}</td>
                  <td>{{$item->total}}</td>
                  <td>
                      <form action="/order/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <input type="submit" onclick="return confirm('Yakin Di Hapus?')" value="delete" class="btn btn-danger btn-sm">
                      </form>

                  </td>


              </tr>


          @empty
              <tr style="text-align: center;"><td colspan="6">Tidak ada Data</td></tr>

          @endforelse


        </tbody>
      </table>
</div>


@endsection
<<<<<<< HEAD
=======
@push('script')
<script src="//cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script>
    $(function() {
        $("#example1").DataTable();
    });
</script>
@endpush
>>>>>>> ebc6d86cd9d4c1aa6c52fbc6f1f02e159078e908
