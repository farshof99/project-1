<footer class="footer_section">
    <div class="container">
       <div class="footer-info">
          <div class="col-lg-7 mx-auto px-0">
             <p>
                &copy; <span id="displayYear"></span> All Rights Reserved By
                <a href="https://html.design/">Free Html Templates</a><br>

                Distributed By <a href="https://themewagon.com/" target="_blank">ThemeWagon</a>
             </p>
          </div>
       </div>
    </div>
 </footer>
