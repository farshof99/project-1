<!DOCTYPE html>
<html>
   <head>
      <!-- Basic -->
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <!-- Mobile Metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <!-- Site Metas -->
      <meta name="keywords" content="" />
      <meta name="description" content="" />
      <meta name="author" content="" />
      @include('sweetalert::alert')
      <link rel="shortcut icon" href="{{ asset("famms/images/favicon.ico") }}" type="">
      <title>OurLook - Fashion Shop</title>
      <!-- bootstrap core css -->
      <link rel="stylesheet" type="text/css" href="{{ asset("famms/css/bootstrap.css") }}" />
      <!-- font awesome style -->
      <link href="{{ asset("famms/css/font-awesome.min.css") }}" rel="stylesheet" />
      <script src="https://kit.fontawesome.com/ac70525fe1.js" crossorigin="anonymous"></script>
      <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Sansita+Swashed&display=swap" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="{{ asset("famms/css/style.css") }}" rel="stylesheet" />
      <!-- responsive style -->
      <link href="{{ asset("famms/css/responsive.css") }}" rel="stylesheet" />
      @stack('style')
   </head>
   <body class="sub_page">
      <div class="hero_area">
         <!-- header section strats -->
            @include('partion.header')
         <!-- end header section -->
      </div>
      <!-- inner page section -->
      <section class="inner_page_head">
         <div class="container_fuild">
            <div class="row">
               <div class="col-md-12">
                  <div class="full">
                      {{-- judul page --}}
                      @yield('judul')
                      {{-- judul page --}}
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- end inner page section -->
      <!-- product section -->
      <section class="layout_padding">
         @yield('content')
      </section>
      <!-- end product section -->
      <!-- footer section -->
        @include('partion.footer')
      <!-- footer section -->
      <!-- jQery -->
      <script src="{{ asset("famms/js/jquery-3.4.1.min.js") }}"></script>
      <!-- popper js -->
      <script src="{{ asset("famms/js/popper.min.js") }}"></script>
      <!-- bootstrap js -->
      <script src="{{ asset("famms/js/bootstrap.js") }}"></script>
      <!-- custom js -->
      <script src="{{ asset("famms/js/custom.js") }}"></script>
      @stack('script')
   </body>
</html>
