<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Komentar;

class KomentarController extends Controller
{
    public function index()
    {
        $komentar = Komentar::all();

        return view('produk.show', compact('komentar'));

    }
    public function testimonial()
    {
        $komentar = Komentar::all();

        return view('testimonial.index', compact('komentar'));

    }
    public function store(Request $request)
    {
        $request->validate([
            'isi' => 'required',
         ]
        );

        
        $komentar = new Komentar;
 
        $komentar->isi = $request->isi;
        $komentar->user_id = Auth::id();
        $komentar->produk_id = $request->produk_id;
        
        $komentar->save();

        return redirect('/produk/'.$komentar->produk_id); 

    }
}
