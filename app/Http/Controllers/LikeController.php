<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Like;

class LikeController extends Controller
{
    public function index()
    {
        
    }
    public function store(Request $request)
    {
        
        $like = new Like;
 
        $like->is_like = $request->is_like;
        $like->user_id = Auth::id();
        $like->produk_id = $request->produk_id;
        
        $like->save();

        return redirect('/produk/'. $like->produk_id); 

    }
}
