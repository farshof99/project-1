<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Produk;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $order = Order::all();

        return view('order.index', compact('order'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = new Order;
        $produk = Produk::find($request->produk_id);

        $order->quantity = $request->quantity;
        $order->total = $request->quantity * $produk->harga;
        $order->user_id = Auth::id();
        $order->produk_id = $request->produk_id;

        $order->save();

        Alert::success('Cart', 'Order Berhasil ditambahkan');

        return redirect('/order');
        // dd($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);

        $order->delete();

        Alert::success('Cart', 'Order Berhasil ditambahkan');
        return redirect('/order');
    }
}
