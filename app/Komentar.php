<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Komentar extends Model
{
    protected $table = "komentar";
    protected $fillable = ["isi", "produk_id", "user_id"];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function produk()
    {
        return $this->belongsTo('App\Produk', 'produk_id');
    }
    public function getCreatedAtAttribute(){
        return Carbon::parse($this->attributes['created_at'])->translatedFormat('l, d F Y');
    }
}
