<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = "produk";
    protected $fillable = ["nama", "harga", "deskripsi", "kategori_id", "foto"];

    public function kategori()
    {
        return $this->belongsTo('App\Kategori', 'kategori_id');
    }
    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }
    public function order()
    {
        return $this->hasMany('App\Order');
    }
    public function like()
    {
        return $this->hasMany('App\Like');
    }

}
